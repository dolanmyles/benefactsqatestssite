from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy_session import current_session as cs
import os
app = Flask(__name__)
app.secret_key = "Secret Key"
import urllib



params = urllib.parse.quote_plus('DRIVER={SQL Server};SERVER=VMTESTDB2.INP.LOCAL;DATABASE=BenefactsQA;Trusted_Connection=yes;')


# SqlAlchemy Database Configuration With

app.config['SQLALCHEMY_DATABASE_URI'] = "mssql+pyodbc:///?odbc_connect=%s" % params

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

from BeneQASub import routes
