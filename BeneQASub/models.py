from BeneQASub import db
from sqlalchemy import MetaData, text, Text, Column

from flask_sqlalchemy import SQLAlchemy


# Creating model table for our CRUD database


class TestDef(db.Model):
    __tablename__ = 'TestDef'
    __table_args__ = {"schema": 'dbo'}
    TestDefId = db.Column(db.Integer, primary_key=True)
    TestName = db.Column(db.String(100))
    TestDesc = db.Column(db.String(200))
    MostRecentTestOutCome = db.Column(db.String(100))
    MostRecentTestDateTime = db.Column(db.String(100))

    def __init__(self, TestDesc, TestName, MostRecentTestOutCome, MostRecentTestDateTime):
        self.TestDesc = TestDesc
        self.TestName = TestName
        self.MostRecentTestOutCome = MostRecentTestOutCome
        self.MostRecentTestDateTime = MostRecentTestDateTime


class TestResult(db.Model):
    __tablename__ = 'TestResult'
    __table_args__ = {"schema": 'dbo'}
    TestResultID = db.Column(db.Integer, primary_key=True)
    TestResultTestDefId = db.Column(db.Integer, db.ForeignKey('dbo.TestDef.TestDefId'), nullable=False)
    TestResultDate=db.Column(db.String(200))
    TestResultOutcome=db.Column(db.String(100))
    TestResultName = db.relationship('TestDef', foreign_keys='TestResult.TestResultTestDefId')

    def __init__(self, TestResultTestDefId,TestResultDate,TestResultOutcome ):
        self.TestResultTestDefId = TestResultTestDefId
        self.TestResultDate = TestResultDate
        self.TestResultOutcome = TestResultOutcome
