from BeneQASub.models import TestDef,TestResult
from BeneQASub import app
from flask import render_template, request, redirect, url_for, flash
from BeneQASub import db
from flask import send_file
from flask import send_from_directory
import subprocess


@app.route('/testdef')
def testdef():
    all_data = TestDef.query.all()
    context = {"testdef_page": "active"} # new info here
    return render_template('testdef.html', testdefs=all_data, context = context)


@app.route('/download/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    print('filen',filename)
    return send_from_directory(directory='C:\\Python\\BeneQA\\BeneQASub\\download\\', filename=filename)




@app.route('/insert_testdef', methods=['POST'])
def insert_testdef():
    if request.method == 'POST':
        TestDesc = request.form['testdesc']
        TestName = request.form['testname']
        MostRecentTestOutCome ="None"
        MostRecentTestDateTime ="None"

        print(TestName,TestDesc)
        my_data = TestDef(TestDesc,TestName,MostRecentTestOutCome,MostRecentTestDateTime)
        db.session.add(my_data)
        db.session.commit()

        flash("Test Def Inserted Successfully")

        return redirect(url_for('testdef'))


# this is our update route where we are going to update our employee
@app.route('/update_testdef', methods=['GET', 'POST'])
def update_testdef():
    if request.method == 'POST':
        my_data = TestDef.query.get(request.form.get('id'))

        my_data.TestDesc = request.form['testdesc']
        my_data.TestName = request.form['testname']

        db.session.commit()
        flash("Test Definition Updated Successfully")

        return redirect(url_for('testdef'))


# This route is for deleting our employee
@app.route('/delete_testdef/<TestDefId>/', methods=['GET', 'POST'])
def delete_testdef(TestDefId):
    my_data = TestDef.query.get(TestDefId)
    db.session.delete(my_data)
    db.session.commit()
    flash("Test Definition Deleted Successfully")

    return redirect(url_for('testdef'))

@app.route('/runps/<TestName>/', methods=['GET', 'POST'])
def runps(TestName):
    print(TestName)
    p = subprocess.Popen(["powershell", "C:\\Python\\BeneQA\\"+TestName+"_wrap\\run.ps1"])
    flash("Test Script Finished",p.communicate())
    return redirect(url_for('testdef'))









@app.route('/')
def testresult():
    all_data = TestResult.query.all()
    my_data2 = TestResult.query.join(TestDef, TestDef.TestDefId == TestResult.TestResultTestDefId).add_columns(TestResult.TestResultID,TestResult.TestResultTestDefId,TestResult.TestResultDate,TestResult.TestResultOutcome,TestDef.TestName)
    context = {"testresult_page": "active"} # new info here
   #return render_template('testresult.html', testresults=all_data, context = context)
    return render_template('testresult.html', testresults=my_data2, context = context)






# this route is for inserting data to sql database via html forms
@app.route('/insert_testresult', methods=['POST'])
def insert_testresult():
    if request.method == 'POST':
        TestResultTestDefId = request.form['TestResultTestDefId']
        TestResultDate=''
        TestResultOutcome=''
        my_data = TestResult(TestResultTestDefId,TestResultDate,TestResultOutcome)
        db.session.add(my_data)
        db.session.commit()

        flash("Test Def Inserted Successfully")

        return redirect(url_for('testresult'))


# this is our update route where we are going to update our employee
@app.route('/update_testresult', methods=['GET', 'POST'])
def update_testresult():
    if request.method == 'POST':
        my_data = TestResult.query.get(request.form.get('TestResultID'))

        my_data.TestResultDate = request.form['TestResultDate']
        my_data.TestResultOutcome = request.form['TestResultOutcome']

        db.session.commit()
        flash("Test Definition Updated Successfully")

        return redirect(url_for('testresult'))


# This route is for deleting our employee
@app.route('/delete_testresult/<TestResultID>/', methods=['GET', 'POST'])
def delete_testresult(TestResultID):
    my_data = TestResult.query.get(TestResultID)


    db.session.delete(my_data)
    db.session.commit()
    flash("Test Definition Deleted Successfully")

    return redirect(url_for('testresult'))